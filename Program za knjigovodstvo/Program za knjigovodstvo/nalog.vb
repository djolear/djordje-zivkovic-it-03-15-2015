﻿Public Class nalog

    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If ComboBox1.SelectedIndex < 0 Then
            MsgBox("Niste izabrali tip naloga", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If MaskedTextBox1.Text = "  .  ." Then
            MsgBox("Niste uneli datum naloga", MsgBoxStyle.Critical)
            Exit Sub
        End If

        TextBox1.Text = (Val(CItajBrojNaloga()) + 1).ToString
        DataGridView1.Enabled = True

    End Sub


    Private Function CItajBrojNaloga()
        Dim broj As String = ""

        FileOpen(1, Form1.DESTINACIJA + "\brojnaloga.lib", OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

        Input(1, broj)
        FileClose(1)

        Return broj

    End Function

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEnter
        Dim udug As Double = 0
        Dim upot As Double = 0
        Dim saldo As Double = 0

        For i = 0 To DataGridView1.Rows.Count - 1
            udug = udug + Val(DataGridView1.Rows(i).Cells(1).Value)
            upot = upot + Val(DataGridView1.Rows(i).Cells(2).Value)

        Next i

        saldo = udug - upot

        TextBox2.Text = udug.ToString
        TextBox3.Text = upot.ToString
        TextBox4.Text = saldo.ToString

    End Sub


    Private Sub SnimanjeNaloga()
        Dim broj As String = ""

        FileOpen(1, Form1.DESTINACIJA + "\knjiga.lib", OpenMode.Append, OpenAccess.Write, OpenShare.Shared)
        Dim izlaz As String = ""
        For i = 0 To DataGridView1.Rows.Count - 2
            If DataGridView1.Rows(i).Cells(0).Value = "" Then DataGridView1.Rows(i).Cells(0).Value = " "
            If DataGridView1.Rows(i).Cells(1).Value = "" Then DataGridView1.Rows(i).Cells(1).Value = "0"
            If DataGridView1.Rows(i).Cells(2).Value = "" Then DataGridView1.Rows(i).Cells(2).Value = "0"
            If DataGridView1.Rows(i).Cells(3).Value = "" Then DataGridView1.Rows(i).Cells(1).Value = "-"
            izlaz = ComboBox1.Text + "," + MaskedTextBox1.Text + "," + TextBox1.Text + "," + DataGridView1.Rows(i).Cells(0).Value.ToString + "," + Val(DataGridView1.Rows(i).Cells(1).Value).ToString + "," + Val(DataGridView1.Rows(i).Cells(2).Value).ToString + "," + DataGridView1.Rows(i).Cells(3).Value.ToString + vbCrLf

            Print(1, izlaz)
        Next
        FileClose(1)



        FileOpen(1, Form1.DESTINACIJA + "\brojnaloga.lib", OpenMode.Output, OpenAccess.Write, OpenShare.Shared)

        Print(1, TextBox1.Text)

        FileClose(1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If Val(TextBox4.Text) <> 0 Then
            MsgBox("Nalog se nezatvara")
            Exit Sub
        End If
        SnimanjeNaloga()
        DataGridView1.Rows.Clear()
        TextBox1.Text = "0"
        DataGridView1.Enabled = False
        Dim v As MsgBoxResult
        v = MsgBox("Zelite li jos neki nalog", MsgBoxStyle.YesNo)
        If v = MsgBoxResult.No Then Me.Close()


    End Sub

    Private Sub nalog_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DataGridView1.Enabled = False

    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.GlavnaKnjigaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NalogZaKnjizenjeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KartceKontaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GlavnaKnjigaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1003, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'GlavnaKnjigaToolStripMenuItem
        '
        Me.GlavnaKnjigaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NalogZaKnjizenjeToolStripMenuItem, Me.KartceKontaToolStripMenuItem})
        Me.GlavnaKnjigaToolStripMenuItem.Name = "GlavnaKnjigaToolStripMenuItem"
        Me.GlavnaKnjigaToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
        Me.GlavnaKnjigaToolStripMenuItem.Text = "Glavna knjiga"
        '
        'NalogZaKnjizenjeToolStripMenuItem
        '
        Me.NalogZaKnjizenjeToolStripMenuItem.Name = "NalogZaKnjizenjeToolStripMenuItem"
        Me.NalogZaKnjizenjeToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.NalogZaKnjizenjeToolStripMenuItem.Text = "Nalog za knjizenje"
        '
        'KartceKontaToolStripMenuItem
        '
        Me.KartceKontaToolStripMenuItem.Name = "KartceKontaToolStripMenuItem"
        Me.KartceKontaToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.KartceKontaToolStripMenuItem.Text = "Kartce konta"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1003, 675)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.Text = "Program za knjigovodstvo "
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents GlavnaKnjigaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NalogZaKnjizenjeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KartceKontaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
